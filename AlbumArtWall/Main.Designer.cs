﻿namespace AlbumArtWall
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bBrowse = new System.Windows.Forms.Button();
            this.tbPathTeste = new System.Windows.Forms.TextBox();
            this.bMake = new System.Windows.Forms.Button();
            this.bRemovePath = new System.Windows.Forms.Button();
            this.bAddPath = new System.Windows.Forms.Button();
            this.lbSelectedFolders = new System.Windows.Forms.ListBox();
            this.lbAllFolders = new System.Windows.Forms.ListBox();
            this.tbLog = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.pPreview = new System.Windows.Forms.Panel();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // bBrowse
            // 
            this.bBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bBrowse.Location = new System.Drawing.Point(1272, 10);
            this.bBrowse.Name = "bBrowse";
            this.bBrowse.Size = new System.Drawing.Size(75, 23);
            this.bBrowse.TabIndex = 0;
            this.bBrowse.Text = "Browse";
            this.bBrowse.UseVisualStyleBackColor = true;
            this.bBrowse.Click += new System.EventHandler(this.bBrowse_Click);
            // 
            // tbPathTeste
            // 
            this.tbPathTeste.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPathTeste.BackColor = System.Drawing.SystemColors.Window;
            this.tbPathTeste.Location = new System.Drawing.Point(12, 12);
            this.tbPathTeste.Name = "tbPathTeste";
            this.tbPathTeste.ReadOnly = true;
            this.tbPathTeste.Size = new System.Drawing.Size(1254, 20);
            this.tbPathTeste.TabIndex = 2;
            // 
            // bMake
            // 
            this.bMake.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bMake.Location = new System.Drawing.Point(874, 708);
            this.bMake.Name = "bMake";
            this.bMake.Size = new System.Drawing.Size(75, 23);
            this.bMake.TabIndex = 5;
            this.bMake.Text = "Make";
            this.bMake.UseVisualStyleBackColor = true;
            this.bMake.Click += new System.EventHandler(this.bMake_Click);
            // 
            // bRemovePath
            // 
            this.bRemovePath.Location = new System.Drawing.Point(456, 327);
            this.bRemovePath.Name = "bRemovePath";
            this.bRemovePath.Size = new System.Drawing.Size(30, 23);
            this.bRemovePath.TabIndex = 7;
            this.bRemovePath.Text = "<<";
            this.bRemovePath.UseVisualStyleBackColor = true;
            this.bRemovePath.Click += new System.EventHandler(this.bRemovePath_Click);
            // 
            // bAddPath
            // 
            this.bAddPath.Location = new System.Drawing.Point(456, 356);
            this.bAddPath.Name = "bAddPath";
            this.bAddPath.Size = new System.Drawing.Size(30, 23);
            this.bAddPath.TabIndex = 8;
            this.bAddPath.Text = ">>";
            this.bAddPath.UseVisualStyleBackColor = true;
            this.bAddPath.Click += new System.EventHandler(this.bAddPath_Click);
            // 
            // lbSelectedFolders
            // 
            this.lbSelectedFolders.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbSelectedFolders.FormattingEnabled = true;
            this.lbSelectedFolders.Location = new System.Drawing.Point(6, 19);
            this.lbSelectedFolders.Name = "lbSelectedFolders";
            this.lbSelectedFolders.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lbSelectedFolders.Size = new System.Drawing.Size(451, 615);
            this.lbSelectedFolders.TabIndex = 9;
            // 
            // lbAllFolders
            // 
            this.lbAllFolders.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbAllFolders.FormattingEnabled = true;
            this.lbAllFolders.Location = new System.Drawing.Point(6, 19);
            this.lbAllFolders.Name = "lbAllFolders";
            this.lbAllFolders.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lbAllFolders.Size = new System.Drawing.Size(430, 303);
            this.lbAllFolders.TabIndex = 10;
            this.lbAllFolders.SelectedIndexChanged += new System.EventHandler(this.lbAllFolders_SelectedIndexChanged);
            // 
            // tbLog
            // 
            this.tbLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbLog.BackColor = System.Drawing.SystemColors.Window;
            this.tbLog.Location = new System.Drawing.Point(6, 19);
            this.tbLog.Multiline = true;
            this.tbLog.Name = "tbLog";
            this.tbLog.ReadOnly = true;
            this.tbLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbLog.Size = new System.Drawing.Size(379, 616);
            this.tbLog.TabIndex = 11;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbLog);
            this.groupBox1.Location = new System.Drawing.Point(956, 48);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(391, 641);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Log";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lbSelectedFolders);
            this.groupBox2.Location = new System.Drawing.Point(486, 48);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(463, 641);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "To use";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lbAllFolders);
            this.groupBox3.Location = new System.Drawing.Point(12, 48);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(442, 331);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "All";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.pPreview);
            this.groupBox4.Location = new System.Drawing.Point(12, 385);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(468, 304);
            this.groupBox4.TabIndex = 15;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Preview";
            // 
            // panel1
            // 
            this.pPreview.Location = new System.Drawing.Point(6, 17);
            this.pPreview.Name = "panel1";
            this.pPreview.Size = new System.Drawing.Size(456, 280);
            this.pPreview.TabIndex = 16;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1359, 743);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.bAddPath);
            this.Controls.Add(this.bRemovePath);
            this.Controls.Add(this.bMake);
            this.Controls.Add(this.tbPathTeste);
            this.Controls.Add(this.bBrowse);
            this.Name = "Main";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bBrowse;
        private System.Windows.Forms.TextBox tbPathTeste;
        private System.Windows.Forms.Button bMake;
        private System.Windows.Forms.Button bRemovePath;
        private System.Windows.Forms.Button bAddPath;
        private System.Windows.Forms.ListBox lbSelectedFolders;
        private System.Windows.Forms.ListBox lbAllFolders;
        private System.Windows.Forms.TextBox tbLog;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Panel pPreview;
    }
}

