﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Collections.ObjectModel;
using System.Threading;

namespace AlbumArtWall
{
    public partial class Main : Form
    {
        ObservableCollection<StringWrapper> folders = new ObservableCollection<StringWrapper>();
        ObservableCollection<StringWrapper> selectedFolders = new ObservableCollection<StringWrapper>();

        public class StringWrapper { public string Value { get; set; } public static StringWrapper FromString(string s) { return new StringWrapper() { Value = s }; } }
        
        public Main()
        {
            //16 albums largura, 6 altura

            InitializeComponent();

            //DEBUG

            lbSelectedFolders.Items.AddRange(new DirectoryInfo(@"G:\Music\Albums\").GetDirectories().Where(x => x.Name.StartsWith("musica")).Select(y => y.FullName).ToArray());
            lbSelectedFolders.Items.AddRange(new DirectoryInfo(@"G:\Music\Albums\").GetDirectories().Where(x => x.Name.StartsWith("Baroness")).Select(y => y.FullName).ToArray());
            lbSelectedFolders.Items.AddRange(new DirectoryInfo(@"G:\Music\Albums\").GetDirectories().Where(x => x.Name.StartsWith("Assopro")).Select(y => y.FullName).ToArray());
            lbSelectedFolders.Items.AddRange(new DirectoryInfo(@"G:\Music\Albums\").GetDirectories().Where(x => x.Name.StartsWith("Elbrus")).Select(y => y.FullName).ToArray());
            lbSelectedFolders.Items.AddRange(new DirectoryInfo(@"G:\Music\Albums\").GetDirectories().Where(x => x.Name.StartsWith("Elder")).Select(y => y.FullName).ToArray());
            lbSelectedFolders.Items.AddRange(new DirectoryInfo(@"G:\Music\Albums\").GetDirectories().Where(x => x.Name.StartsWith("Electric Wizard")).Select(y => y.FullName).ToArray());
            lbSelectedFolders.Items.AddRange(new DirectoryInfo(@"G:\Music\Albums\").GetDirectories().Where(x => x.Name.StartsWith("Graveyard")).Select(y => y.FullName).ToArray());
            lbSelectedFolders.Items.AddRange(new DirectoryInfo(@"G:\Music\Albums\").GetDirectories().Where(x => x.Name.StartsWith("Equations")).Select(y => y.FullName).ToArray());
            lbSelectedFolders.Items.AddRange(new DirectoryInfo(@"G:\Music\Albums\").GetDirectories().Where(x => x.Name.StartsWith("Kyuss")).Select(y => y.FullName).ToArray());
            lbSelectedFolders.Items.AddRange(new DirectoryInfo(@"G:\Music\Albums\").GetDirectories().Where(x => x.Name.StartsWith("Steven Wilson")).Select(y => y.FullName).ToArray());
            lbSelectedFolders.Items.AddRange(new DirectoryInfo(@"G:\Music\Albums\").GetDirectories().Where(x => x.Name.StartsWith("The Doors")).Select(y => y.FullName).ToArray());
            lbSelectedFolders.Items.AddRange(new DirectoryInfo(@"G:\Music\Albums\").GetDirectories().Where(x => x.Name.StartsWith("Wo")).Select(y => y.FullName).ToArray());
            lbSelectedFolders.Items.AddRange(new DirectoryInfo(@"G:\Music\Albums\").GetDirectories().Where(x => x.Name.StartsWith("Uncle")).Select(y => y.FullName).ToArray());

            lbSelectedFolders.Items.AddRange(new DirectoryInfo(@"G:\Music\Discografias\").GetDirectories().Where(x => x.Name.StartsWith("God")).Select(y => y.FullName).ToArray());
            lbSelectedFolders.Items.AddRange(new DirectoryInfo(@"G:\Music\Discografias\").GetDirectories().Where(x => x.Name.StartsWith("Goj")).Select(y => y.FullName).ToArray());
            lbSelectedFolders.Items.AddRange(new DirectoryInfo(@"G:\Music\Discografias\").GetDirectories().Where(x => x.Name.StartsWith("Om")).Select(y => y.FullName).ToArray());
            lbSelectedFolders.Items.AddRange(new DirectoryInfo(@"G:\Music\Discografias\").GetDirectories().Where(x => x.Name.StartsWith("Stoned Jesus")).Select(y => y.FullName).ToArray());
            lbSelectedFolders.Items.AddRange(new DirectoryInfo(@"G:\Music\Discografias\").GetDirectories().Where(x => x.Name.StartsWith("Them")).Select(y => y.FullName).ToArray());
            lbSelectedFolders.Items.AddRange(new DirectoryInfo(@"G:\Music\Discografias\").GetDirectories().Where(x => x.Name.StartsWith("Truck")).Select(y => y.FullName).ToArray());
        }

        private void bBrowse_Click(object sender, EventArgs e)
        {
            var fbd = new FolderBrowserDialog();

            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                var path = fbd.SelectedPath;

                DirectoryInfo d = new DirectoryInfo(path);

                lbAllFolders.Items.Clear();

                lbAllFolders.Items.AddRange(d.EnumerateDirectories().Select(x => x.FullName).OrderBy(x=>x).ToArray());
            }
        }

        private Image GetWallpaper(IEnumerable<Image> covers, int width, int height, int staticSize = 120)
        {
            int wCursor = 0;
            int hCursor = 0;

            int coverRowCount = 0;

            Bitmap wallpaper = new Bitmap(width, height);
            using (Graphics g = Graphics.FromImage(wallpaper))
            {
                foreach (var cover in covers)
                {

                    var ratio = (double)(240*0.90) / (double)cover.Width;
                    if (ratio < (160 * 0.90) / cover.Height) ratio = (double)(160 * 0.90) / (double)cover.Height;

                    //var cov = cover.GetThumbnailImage(Convert.ToInt32(cover.Width * ratio), Convert.ToInt32(cover.Height * ratio), null, IntPtr.Zero);
                    var cov = cover.Downsize(staticSize, staticSize);


                    wCursor = cov.Width * coverRowCount;
                    if (wCursor > width)
                    {
                        wCursor = 0;
                        hCursor += cov.Height;
                        coverRowCount = 0;
                    }

                    g.DrawImage(cov, wCursor, hCursor);
                    coverRowCount++;
                    if (hCursor == 0 && wCursor == 0) wCursor = cov.Width * coverRowCount;

                    if (hCursor > height && wCursor > width) return wallpaper;
                }
            }

            return wallpaper;
        }

        private void ToLog(string s)
        {
            Thread t = new Thread(() =>
            {
                if (this.InvokeRequired)
                {
                    this.Invoke((MethodInvoker)delegate() { tbLog.Text += string.Format("{0}{1}", s, Environment.NewLine); });
                }
                else
                {
                    tbLog.Text += string.Format("{0}{1}", s, Environment.NewLine);
                }
            }
            );
            
            t.Start();
        }

        private void bMake_Click(object sender, EventArgs e)
        {
            try
            {
                var covers = new List<Image>();
                var dirs = new List<DirectoryInfo>();

                foreach (var dir in lbSelectedFolders.Items.OfType<object>().Select(x=>x.ToString()))
                {
                    var files = Directory.GetFiles(dir, "*.mp3", SearchOption.AllDirectories);

                    if (files.Count() > 0)
                    {
                        try
                        {
                            var distinctByAlbum = files.Select(y => TagLib.File.Create(y)).GroupBy(x => x.Tag.Album).Select(x => x.First()).ToList();

                            foreach (var mp3 in distinctByAlbum)
                            {
                                if (mp3.Tag.Pictures.Count() > 0) covers.Add(Image.FromStream(new MemoryStream(mp3.Tag.Pictures[0].Data.Data)));

                                ToLog(string.Format("{0} - {1}, {2}", covers.Count, mp3.Tag.FirstArtist, mp3.Tag.Album));
                            }
                        }
                        catch (Exception ex)
                        {
                            continue;
                        }
                        
                    }
                }

                //var wallpaper = GetWallpaper(covers, 1920, 1080);
                var wallpaper = GetWallpaper(covers.Shuffle(), 1920, 1080);

                var sfd = new SaveFileDialog();

                if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    wallpaper.Save(sfd.FileName, System.Drawing.Imaging.ImageFormat.Png);

                    System.Diagnostics.Process.Start(sfd.FileName);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private void bRemovePath_Click(object sender, EventArgs e)
        {
            if (lbSelectedFolders.SelectedItems.Count == 0) return;

            var toRemove = new List<string>();

            foreach (var item in lbSelectedFolders.SelectedItems)
            {
                toRemove.Add(item.ToString());
            }

            foreach (var item in toRemove)
            {
                lbSelectedFolders.Items.Remove(item);
            }
        }

        private void bAddPath_Click(object sender, EventArgs e)
        {
            if (lbAllFolders.SelectedItems.Count == 0) return;
            foreach (var item in lbAllFolders.SelectedItems)
            {
                if(!lbSelectedFolders.Items.Contains(item))
                {
                    lbSelectedFolders.Items.Add(item);
                }
            }
        }

        private void lbAllFolders_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selected = lbAllFolders.SelectedItem.ToString();
            var files = Directory.GetFiles(selected, "*.mp3", SearchOption.AllDirectories);

            if (files.Count() > 0)
            {
                try
                {
                    var distinctByAlbum = files.Select(y => TagLib.File.Create(y)).GroupBy(x => x.Tag.Album).Select(x => x.First()).ToList();
                    var img = new List<Image>();                    

                    foreach (var mp3 in distinctByAlbum)
                    {
                        if (mp3.Tag.Pictures.Count() > 0) img.Add(Image.FromStream(new MemoryStream(mp3.Tag.Pictures[0].Data.Data)));
                    }

                    if (img.Count > 0) pPreview.BackgroundImage = GetWallpaper(img, pPreview.Width, pPreview.Height, Convert.ToInt32(Math.Sqrt((((double)pPreview.Width * (double)pPreview.Height) / img.Count))));
                }
                catch (Exception ex)
                {
                }

            }
        }
    }
}
